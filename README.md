# about-me

## ¿Algo que considero que me hace diferente?

For the last 20 years I have always worked in teams where communication and teamwork were very important. Maybe that experience make me different...

## ¿Qué es para mí Git?

I think that is the best way to work in remote mode with others team members

## ¿Qué es para mí Docker?

Is a tool to create a virtual image of a complete web site or application to run on any PC easily without having to install any other program or library to make it work.

## ¿Que es para mí el Testing?

Is checking the correct functionally of the code that we created with a specific framework to make it.

## ¿Qué características de JavaScript conozco ahora mismo?

Language oriented to objects. It can manipulate Dom elements and works with them. Only can do one thing at time for that we use asynchrony to work with Javascript.

## ¿Qué son para mí los Web Components?

The web Components are customs components created with Javascript and Dom elements, to be used in html.

## Cualquier otra cosa que quieras compartir que consideres que te hace única

I don't consider myself a special person but maybe having a daughter makes me to see the life in other way.
